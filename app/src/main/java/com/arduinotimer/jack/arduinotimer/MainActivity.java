package com.arduinotimer.jack.arduinotimer;

import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Queue;
import java.util.Stack;


// Leave a timer running somewhere without having to leave your phone there.
public class MainActivity extends AppCompatActivity {

    Button[] numberInputButtons = new Button[10];
    BluetoothHandler bToothHandler;
    int toneCode = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bToothHandler = new BluetoothHandler(this, getIntent().getExtras());

        final TimeBuilder timeBuilder = new TimeBuilder();
        final TextView timeBuilderText = findViewById(R.id.timeBuilderText);

        ImageButton backSpaceButton = findViewById(R.id.backspaceButton);
        backSpaceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String result = timeBuilder.removeNumber();
                if (!result.isEmpty()) {
                    timeBuilderText.setText(result);
                }
            }
        });

        int buttonCounter = 0;
        ViewGroup parentView = findViewById(R.id.numberInputTable);
        for (int i = 0; i < parentView.getChildCount(); i++) {
            ViewGroup row = (ViewGroup) parentView.getChildAt(i);
            for (int j = 0; j < row.getChildCount(); j++) {
                numberInputButtons[buttonCounter++] = (Button) row.getChildAt(j);
            }
        }

        for (int i = 0; i < numberInputButtons.length; i++) {
            numberInputButtons[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // insert number to number view.
                    TextView tView = (TextView) v;
                    String result = timeBuilder.addNumber(Integer.parseInt(tView.getText().toString()));
                    if (!result.isEmpty()) {
                        timeBuilderText.setText(result);
                    }
                }
            });
        }


        Button setButton = findViewById(R.id.setButton);
        setButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int totalSeconds = timeBuilder.getTotalSeconds() + 1; // + 1 just an offset probably should be done on the arduino but I'm doing it here.
                timeBuilderText.setText(getResources().getString(R.string.defaultTimeString));
                System.out.println(totalSeconds + ":" + toneCode + "@");
                bToothHandler.writeToConnectedThread(totalSeconds + ":" + toneCode + "@");
            }
        });


        Spinner toneCodeSpinner = findViewById(R.id.toneCodeSpinner);
        Integer[] items = new Integer[]{1,2,3,4,5,6};
        final ArrayAdapter<Integer> adapter = new ArrayAdapter<>(this, R.layout.spinner_item, items);
        toneCodeSpinner.setAdapter(adapter);

        toneCodeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Object item = adapterView.getItemAtPosition(i);
                toneCode = (int) item;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

    }

    class TimeBuilder {

        LinkedList<Integer> columns;
        final int COLUMN_LIMIT = 6;

        public TimeBuilder() {
            columns = new LinkedList<>();
        }

        public String addNumber(int num) {
            if (columns.size() < COLUMN_LIMIT) {
                columns.push(num);
                return createTimeString();
            } else {
                return "";
            }
        }

        public String removeNumber() {
            if (!columns.isEmpty()) {
                columns.removeFirst();
                return createTimeString();
            } else {
                return "";
            }
        }

        public String createTimeString() {

            Integer[] buffer = new Integer[columns.size()];

            String result = "5#4#h 3#2#m 1#0#s";
            int counter = 0;
            while (!columns.isEmpty()) {
                buffer[counter] = columns.pop();
                result = result.replace(counter + "#", buffer[counter].toString());
                counter++;
            }

            result = result.replaceAll("\\d#", "0");

            for (Integer val : buffer) {
                columns.addLast(val);
            }
            return result;
        }

        public int getTotalSeconds() {

            int total = 0;

            while (!columns.isEmpty()) {
                Integer num = columns.removeLast();
                num *= getColumnSecondsMultiplier(columns.size());
                total += num;
            }
            return total;
        }

        private int getColumnSecondsMultiplier(int index) {
            if (index == 0) {
                return 1;
            } else if (index == 1) {
                return 10;
            } else if (index == 2) {
                return 60;
            } else if (index == 3) {
                return 600;
            } else if (index == 4) {
                return 3600;
            } else if (index == 5) {
                return 36000;
            } else {
                return 0;
            }
        }

        private class TimeColumn {

            final String type;
            int value = 0;

            public TimeColumn(String type) {
                this.type = type;
            }
        }

    }
}
