package com.arduinotimer.jack.arduinotimer;

import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.List;

public class TimerActivity extends AppCompatActivity implements TimerPickerFragment.OnTimePickedListener {

    public BluetoothHandler bToothHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer);

        Button setButton = findViewById(R.id.setButton);
        setButton.setOnClickListener(new SetTimerButtonListener());
        bToothHandler = new BluetoothHandler(this, getIntent().getExtras());

        Button stopNoiseButton = findViewById(R.id.stopNoiseButton);
        stopNoiseButton.setOnClickListener(new SetStopNoiseButtonListener());
    }

    @Override
    public void onTimePicked(String timeString) {
        System.out.println(timeString);
        bToothHandler.writeToConnectedThread("1");
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        if (fragment instanceof TimerPickerFragment) {
            TimerPickerFragment headlinesFragment = (TimerPickerFragment) fragment;
            headlinesFragment.setOnTimePickedListener(this);
        }
    }

    private class SetTimerButtonListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            DialogFragment timerPickFrag = new TimerPickerFragment();
            timerPickFrag.show(getSupportFragmentManager(), "timePicker");
        }
    }

    private class SetStopNoiseButtonListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            bToothHandler.writeToConnectedThread("0");
        }
    }
}
