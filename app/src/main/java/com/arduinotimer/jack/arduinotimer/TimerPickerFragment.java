package com.arduinotimer.jack.arduinotimer;

import android.app.Activity;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.widget.TimePicker;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Calendar;

public class TimerPickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

    OnTimePickedListener mCallback;

    public void setOnTimePickedListener(Activity activity) {
        mCallback = (OnTimePickedListener) activity;
    }

    // Container Activity must implement this interface
    public interface OnTimePickedListener {
        void onTimePicked(String timeString);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        return new TimePickerDialog(getActivity(), this, hour, minute, true);
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        // Send time back to main activity
        String timeString = "#"+hourOfDay + "+" + minute + "#";
        mCallback.onTimePicked(timeString);
    }
}
