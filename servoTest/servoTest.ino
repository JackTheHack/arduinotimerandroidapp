#include <Servo.h>

const float c4 = 261.63;
const float c4s = 277.18;
const float d4 = 293.66;
const float d4s = 311.13;
const float e4 = 329.63;
const float f4 = 349.23;
const float f4s = 369.99;
const float g4 = 392.00;
const float g4s = 415.30;
const float a4 = 440.00;
const float a4s = 466.16;
const float b4 = 493.88;
const float c5 = 523.25;
const float c5s = 554.37;
const float d5 = 587.33;
const float SILENCE = -1;

//const String uuid = "2e2039d5-c02e-414d-be85-c40666b3afdb";

//class Note {
//
//  float freq;
//  float dur;
//
//  public:
//    Note(float frequency, int duration) {
//      freq = frequency;
//      dur = duration;
//    }
//    
//};

class NotePlayer {

  int buzzerPin;
  float currentFreq;
  int currentDuration;
  int silenceDuration;

  int updateInterval;
  unsigned long previousUpdate;

  boolean playingNote = false;
  
  public:
    NotePlayer(int pin) {
      buzzerPin = pin;
      pinMode(buzzerPin, OUTPUT);
      currentFreq = c4;
      currentDuration = 200;
      silenceDuration = 800;
    }

    void Update() {

      if (playingNote) {
        if ((millis() - previousUpdate) > currentDuration) {
          previousUpdate = millis();
          noTone(buzzerPin);
          playingNote = false;
        }
        return;
      }
      
      if ((millis() - previousUpdate) > silenceDuration) {
        
        previousUpdate = millis();

        
        tone(buzzerPin, currentFreq);
  
        if (currentFreq == c4) {
          currentFreq = g4;
        } else if (currentFreq == g4) {
          currentFreq = c4;
        }
      
        
        playingNote = true;
      }
    }

    void playNote(float note, float duration) {
      tone(buzzerPin, note);
      delay(duration);
    }
    
    void playSilence(float duration) {
      noTone(buzzerPin);
      delay(duration);
    }
    
    void playStartupTone() {
      playNote(c4, 100);
      playSilence(80);
      playNote(d4, 100);
      playNote(e4, 100);
      playSilence(60);
      playNote(f4, 100);
      playNote(g4, 100);
      playSilence(80);
      playNote(f4, 100);
      playNote(e4, 100);
      playSilence(60);
      playNote(d4, 100);
      playNote(c4, 500);
      noTone(buzzerPin);
    }

    void playTone1() {
      playNote(d4, 100);
      playSilence(80);
      playNote(e4, 100);
      playNote(f4, 100);
      playSilence(60);
      playNote(g4, 100);
      playNote(a4, 100);
      playSilence(80);
      playNote(b4, 100);
      playNote(a4, 100);
      playSilence(60);
      playNote(e4, 100);
      playNote(d4, 500);
      noTone(buzzerPin);
    }

    void playTone2() {

      for (int i = 0; i < 10; i++) {
        playNote(e4, 300);
        playSilence(200);  
      }
      noTone(buzzerPin);
    }

    void playTone3() {

      for (int i = 5; i > 0; i--) {
        int dur = (i * 100) + 25;
        playNote(c4, dur);
        playSilence(75);
        playNote(e4, dur);
        playSilence(75);
        playNote(g4, dur);
        playSilence(75);
        playNote(c5, dur);
        playSilence(75);
        playNote(g4, dur);
        playSilence(75);
        playNote(e4, dur);
        playSilence(75);
      }
      playNote(c4, 300);
      
      noTone(buzzerPin);
    }

    void playTone4() {

      for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 10; j++) {
          playNote(e4, 75);
          playNote(f4, 75);
          playSilence(40);  
        }
        playNote(e4, 1000);
        playSilence(3000);  
      }
      noTone(buzzerPin);
    }

    void playTone5() {

      for (int i = 0; i < 5; i++) {
        playNote(d5, 200);
        playSilence(75);
        playNote(c5, 175);
        playSilence(50);
        playNote(a4, 175);
        playSilence(75);
        playNote(a4s, 175);
        playSilence(50);
        playNote(b4, 800);
        playSilence(100);
        
        playNote(d5, 200);
        playSilence(75);
        playNote(c5, 200);
        playSilence(50);
        playNote(b4, 400);
        playSilence(100);
  
        playNote(a4, 200);
        playSilence(75);
        playNote(a4s, 200);
        playSilence(50);
        playNote(b4, 400);
        playSilence(100);
  
        playNote(a4, 200);
        playSilence(75);
        playNote(b4, 200);
        playSilence(50);
        playNote(c5, 800);
        playSilence(3000);  
      }
      noTone(buzzerPin);
    }

    void playTone6() {
      for (int i = 0; i < 3; i++) {
        for (int j = 0 ; j < 3; j++) {
          playNote(c4, 150);
          playSilence(45);
          playNote(c4, 75);
          playSilence(45);
          playNote(c4, 75);
          playSilence(45);
          playNote(c4, 150);
          playSilence(90);
        }
        playNote(c4, 150);
        playSilence(90);
        playNote(c4, 150);
        playSilence(90);
        playNote(d4, 150);
        playSilence(90);
        
        for (int i =0 ; i < 2; i++) {
          playNote(d4s, 150);
          playSilence(45);
          playNote(d4s, 75);
          playSilence(45);
          playNote(d4s, 75);
          playSilence(45);
          playNote(d4s, 150);
          playSilence(90);
        }
        playNote(f4, 170);
        playSilence(90);
        playNote(f4, 75);
        playSilence(45);
        playNote(f4, 75);
        playSilence(45);
        playNote(f4, 150);
        playSilence(90);
  
        playNote(d4s, 150);
        playSilence(90);
        playNote(d4s, 150);
        playSilence(90);
  
        playNote(d4, 150);
        playSilence(90);
      }
      playNote(c4, 400);
      noTone(buzzerPin);
      
    }

    void playNotes(int toneCode) {
      if (toneCode == 0) {
        playStartupTone();
      } else if (toneCode == 1) {
        playStartupTone();
      } else if (toneCode == 2) {
        playTone2();
      } else if (toneCode == 3) {
        playTone3();
      } else if (toneCode == 4) {
        playTone4();
      } else if (toneCode == 5) {
        playTone5();
      } else if (toneCode == 6) {
        playTone6();
      }
    }
};


class Sweeper {
  Servo servo;
  int pos;
  int increment;
  int updateInterval;
  int motionInterval;
  int lowSafe;
  int highSafe;
  int currentCountDown;
  int startingCount;
  int absoluteCountDown;
  unsigned long previousLogicUpdate;
  unsigned long previousMotionUpdate;

  int servoPin;

  boolean active = false;

  public:
    Sweeper(int pin, int interval, int countDown, int lSafe, int hSafe) {
      servoPin = pin;
      updateInterval = interval;
      lowSafe = lSafe;
      highSafe = hSafe;
    }

    boolean finished = false;

    void setCountDownSeconds(int val) {
      absoluteCountDown = val;
      currentCountDown = val;
      startingCount = val;

//      updateInterval = (int) ((highSafe - lowSafe) / (absoluteCountDown * 1.0)) * 1000;
      if (absoluteCountDown < (highSafe - lowSafe) - 1) {
        motionInterval = 1000;
      } else {
        motionInterval = ((absoluteCountDown * 1.0) / (highSafe - lowSafe)) * 1000;
      }
      calculateIncrement();
      
      pos = lowSafe;
      active = true;
      finished = false;
      Attach();
    }

    void Attach() {
      servo.attach(servoPin);
    }

    void Detach() {
      servo.detach();
    }

    int calculateIncrement() {
      // Set increment to a minimum of 1, countdowns less than 180 will
      // use larger steps.
      if (absoluteCountDown < (highSafe - lowSafe)) {
        increment = (highSafe - lowSafe) / absoluteCountDown; // minimum of 1 but can be more.
      } else {
        increment = 1;  
      }
    }

    void Update() {
      if (active && (millis() - previousLogicUpdate) > updateInterval) {

        if (absoluteCountDown <= 0) {
          active = false;
          finished = true;
          Detach();
        }
        
        previousLogicUpdate = millis();
//        pos += increment;
        absoluteCountDown -= 1;
        servo.write(pos);
//        Serial.println(pos);
        if ((pos >= highSafe) || (pos <= lowSafe)) {
          currentCountDown = startingCount;
        }
      }

      if (active && (millis() - previousMotionUpdate) > motionInterval) {
        
        previousMotionUpdate = millis();

        pos += increment;
        if ((pos >= highSafe) || (pos <= lowSafe)) {
          currentCountDown = startingCount;
          pos = lowSafe; 

          calculateIncrement();
        }
      }
    }
};

int potpin = 0;
int val;

int LDRPin = 0;
int LDRValue = 0;

const int upperSafe = 174;
const int lowerSafe = 15;
int startingCount = 60;
int countDown = startingCount;

Sweeper servo1(9, 1000, 0, lowerSafe, upperSafe);
NotePlayer notePlayer(13);
//const int buzzerPin = 13;

int state = 0;
int flag = 0;
String inString;

int toneCode = 0;


void setup()
{
  Serial.begin(9600);
  notePlayer.playNotes(0);
  servo1.Attach();
  
}

void loop()
{

  while (Serial.available() > 0) {

    int seconds = Serial.parseInt();
    toneCode = Serial.parseInt();
    
    if (Serial.read() == '@') {
      servo1.setCountDownSeconds(seconds);
    }
    Serial.println(seconds);
    Serial.println(toneCode);
//    int inChar = Serial.read();
//    
//    if (isDigit(inChar)) {
//      // convert the incoming byte to a char and add it to the string:
//      inString += (char)inChar;
//    }
//    else if (isAlpha(inChar)) {
//      // detect separator for buzzer tone class
//      if ((char) inChar == '#') {
//        toneCode = Serial.read();
//        Serial.println(toneCode); 
//      }
//       
//    }
//    
//    // detect end of input
//    if (inChar == '@') {
//      Serial.println(inString.toInt());
//      // clear the string for new input:
//      servo1.setCountDownSeconds(inString.toInt());
//      inString = "";
//    }
  }

  servo1.Update();

  if (servo1.finished) {
    notePlayer.playNotes(toneCode);
    servo1.finished = false;
  }

//  if (Serial.available() > 0) {
//    state = Serial.read();
//    flag = 0;
//  }
//  if (state == '0') {
//    noTone(13);
//    if (flag == 0) {
////      Serial.println("Buzzer off");
//      flag = 1;
//    }
//  } else if (state == '1') {
//    tone(13, 300);
//    if (flag == 0) {
////      Serial.println("Buzzer on");
//      flag = 1;
//    }
//  }
  
//  player1.Update();
  
}

// Read from Ldr
//  val = analogRead(potpin);  
//           
//  Serial.println(val);         
//  val = map(val, 0, 1023, 0, 179); 
//  Serial.println(val);
